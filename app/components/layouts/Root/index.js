import React from 'react';
import { RouteHandler } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';

require('./style.scss');
injectTapEventPlugin();

class Root extends React.Component {

  render () {
    return (
      <div className="Root">
        <RouteHandler />
      </div>
    );
  }

}

export default Root;
