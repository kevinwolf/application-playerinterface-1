import { createStore, bind, datasource } from 'alt/utils/decorators';
import flux from 'flux';
import ApiService from 'services/ApiService';
import AuthActions from 'actions/AuthActions';

@createStore(flux)
@datasource(ApiService)
class AuthStore {

  constructor () {
    this.state = { token : null };
  }

  @bind(AuthActions.authenticate)
  authenticate (params) {
    this.getInstance().authenticate(params);
  }

  @bind(AuthActions.setToken)
  login (token) {
    localStorage.setItem('token', token);
    this.setState({ token : token });
  }

  @bind(AuthActions.logout)
  logout () {
    localStorage.removeItem('token');
    this.setState({ token : null });
  }

}

export default AuthStore;
