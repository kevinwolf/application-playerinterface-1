import axios from 'axios';
import AuthActions from 'actions/AuthActions';

const API_ENDPOINT = `${API_URL}/api`;

const ApiService = {
  authenticate : {
    success : AuthActions.loginSuccessful,
    error   : AuthActions.loginFailed,
    remote (state, params) {
      return axios.post(`${API_ENDPOINT}/players/auth`, params);
    },
  },
};

export default ApiService;
